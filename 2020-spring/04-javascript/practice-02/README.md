# Practice 02. 사진 올리기 페이지

<a href="https://fastcampus-all.netlify.com/2020-spring/04-javascript/practice-03" target="_blank">이번강 완성본 보기</a>
<br>
<a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/04-javascript/practice-02/practice-02.zip" target="_blank">
  [실습파일 다운로드 받기]
</a>
<br>
<br>

> `내 정보` 탭으로 초기화 

```html
<li class="upload on" onclick="setMenu('upload')">사진 올리기</li>
```
```html
<main class="upload">
```

<br>

***

## 입력칸의 글자 수 우측에 표시하기
* `input`에 글자를 입력할 때마다 로직 실행  
* `input`의 `value`값을 인지하여 우측 span의 `innerHTML` 변경


<br>

### 입력칸의 글자입력 감지하기
| onkeypress | onkeydown | onkeyup |  
| :-- | :-- | :-- |
| 입력 **전** 이벤트 발생 *(문자키만 인식)* | 입력 **전** 이벤트 발생 | 입력 **후** 이벤트 발생 | 

<br>

```
onkeyup="console.log('key up')"
```

<br>

### 함수로 설정하여 버튼에 연결
```javascript
function setDescLength () {
  document.querySelector(".descLength").innerHTML =
   document.querySelector("input.description").value.length + "/20";
}
```
```html
<input class="description" type="text" maxlength="20" placeholder="사진 설명을 20자 이내로 적어주세요." onkeyup="setDescLength()"/>
```

<br>

***

<br>

## 다른 방법으로 버튼과 함수 연결
`event listener` 사용해보기

<br>

***

<br>

## 다음 강좌  
* [03. 내 정보 수정 & 저장하기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/practice-03/README.md)