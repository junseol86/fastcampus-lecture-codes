# 01. CSS 소개와 사용 방법
`CSS` : **C**ascade **S**tyle **S**heet

<br>

| HTML | **CSS** | Javascript |
| :-- | :-- | :-- |
| 갖다놓고 | **꾸미고** | 시킨다 |

<br>

## 우리가 CSS를 사용하는 이유
1. 웹사이트를 내가 원하는대로 꾸밀 수 있다.  
2. `HTML을 HTML답게` 사용할 수 있다.

<br>

### <a href="http://www.csszengarden.com/" target="_blank">[CSS Zen Garden]</a> 둘러보기
> 같은 html 파일을 CSS로 다양하게 디자인한 예시들

<br>

### **정보 요소와 시각 요소의 분리**
> 설계는 `HTML`로 & 디자인은 `CSS로`

<br>
<br>

***

<br>
<br>

## CSS 코드 사용하기

```html
<span>이 부분</span>을 두껍고 파랗게!
```

<br>

### `사용법 1.` 요소에 직접 적용하기
```html
<span style="font-weight: bold; color: blue;">이 부분</span>을 두껍고 파랗게!
```

<br>

### `사용법 2.` head 태그 내 style 태그에 적용
```html
<head>
  <!-- 중략 -->

  <style>
    span {
      font-weight: bold;
      color: blue;
    }
  </style>

</head>
<body>

  <span>이 부분</span>을 두껍고 파랗게!

</body>
```

<br>

### `사용법 3.` 별도의 CSS 파일에 분리
```html
<head>

  <!-- 중략 -->
  <link rel="stylesheet" type="text/css" href="style.css"/> 

</head>
<body>

  <span>이 부분</span>을 두껍고 파랗게!

</body>
```

```css
span {
  font-weight: bold;
  color: blue;
}
```

<br>
<br>

***

<br>

## 이번 챕터에서 활용할 사이트/툴

![JS Fiddle](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/03-css/01/images/jsfiddle-logo.png)  
> <a href="https://jsfiddle.net/" target="_blank">[JS Fiddle 열기]</a>  

<br>

또는 VS Code Extension `Live Server Preview`

<br>
<br>

***

<br>
<br>

## 다음 강좌
* [02. CSS 선택자](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/03-css/02/README.md)