# 06. HTML 중첩되는 태그
다른 태그(들)을 포함하거나 다른 태그에 포함되어 사용되는 태그들입니다.

<br>

## 목록 태그
```
어떤 요소들을 포함하는 목록을 나타낼 때 사용합니다.
```
  
`ul` > `li`  
`ol` > `li`

<br>

### `<ul>` : **unordered**(순서가 없는) list
|속성|역할|값|
|:--|:--|:--|
|type|항목 앞의 표시자 모양|`disc`(기본), `square`, `circle`...|

<br>
   
### `<ol>` : **ordered**(순서가 있는) list
|속성|역할|값|
|:--|:--|:--|
|type|항목 앞의 표시자 형태|`1`, `A`, `a`, `I`, `i`...|
|start|시작할 번호|**정수 숫자**값 (type과 관계없이)|

<br>

### `<li>` : list **item** - 목록의 요소
* `★` 다른 태그들(또 다른 목록 등)을 포함할 수 있음.

<br>

### [예제]
![목록 태그 예제 결과](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/02-html/06/screenshots/list.png)   

* <a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/blob/master/2020-spring/02-html/06/list-lecture.html" target="_blank">목록 예제 실습 코드</a>
* <a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/blob/master/2020-spring/02-html/06/list-complete.html" target="_blank">목록 예제 완성본 코드</a>

<br>

> `포인트` 목록 태그는 CSS와 함께 다양한 형태로 활용됨
> * 예: 사이트의 메뉴 항목들

<br>

***

<br>

## 테이블 태그
`table` > `tr` > `th`, `td`  
`table` > `caption`

<br>

### `<table>` : **table** - 표

<br>

### `<tr>` : **table row** - 표의 한 열

<br>

### `<th>` : **table header** - 각 열, 행의 머리
|속성|역할|값|
|:--|:--|:--|
|scope|무엇(행/열)의 머리인지 표시|`col`, `row`...|

<br>

### `<td>` : **table data** - 표의 각 칸   
|속성|역할|값|
|:--|:--|:--|
|colspan|가로로 n개 칸 병함|칸 개수 숫자값|
|rowspan|세로로 n개 칸 병함|칸 개수 숫자값|
`★` 다른 태그들을 포함할 수 있음.


<br>

### `<caption>` : 표의 제목

<br>

### [예제]
![테이블 태그 예제 결과](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/02-html/06/screenshots/table.png)

* <a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/blob/master/2020-spring/02-html/06/table-lecture.html" target="_blank">테이블 예제 실습 코드</a>
* <a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/blob/master/2020-spring/02-html/06/table-complete.html" target="_blank">테이블 예제 완성본 코드</a>

<br>

> `주의` 레이아웃, 디자인 용도로 사용하지 말 것!
> * 위 목적으로는 이후에 배울 CSS를 사용하세요.

<br>

***

<br>

## 다음 강좌
* [07. HTML 인라인 요소와 블럭 요소](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/02-html/07/README.md)