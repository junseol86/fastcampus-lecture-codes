# 03. Firebase 스토리지
파일들을 업로드하고 관리


<br>

## 문서 둘러보기
* `Cloud Storage`의 `웹` 항목 살펴보기


***

<br>

## 시작하기
> Firebase 콘솔에서 `Storage` 탭 이동  

* 시작 후 파일들 업로드한 뒤 접근해보기
* 규칙: `false`를 `true`로 변경

<br>

### 사이트에서 스토리지 접근

1. 스크립트 추가
```html
  <script src="https://www.gstatic.com/firebasejs/버전/firebase-storage.js"></script>
```
```javascript
var storage = firebase.storage();
```

<br>


2. 파일 업로드

> 함수 만들기
```javascript
function uploadFile () {
  // ...
}
```

<br>

> input으로부터 파일 받아오기
```javascript
function uploadFile () {
  var file = document.querySelector("input[type=file]").files[0];
}
```

<br>

> 받아온 파일 올리기
```javascript
function uploadFile () {
  // ...
  var ref = storage.ref().child(file.name);
  ref.put(file).then(function(snapshot) {
    console.log(snapshot);
  })
}
```

<br>

> 버튼에 연결하기
```html
<div class="button" onclick="uploadFile();">업로드</div>
```

> 업로드한 뒤 해당 파일의 링크 확인
```javascript
function uploadFile () {
  var file = document.querySelector("input[type=file]").files[0];
  var ref = storage.ref().child(file.name);
  ref.put(file).then(function(snapshot) {
    snapshot.ref.getDownloadURL().then(function (url) {
      var downloadUrl = url;
      console.log(downloadUrl);
    })
  });
}
```

<br>

***

<br>

## 다음 강좌
* [04. Firebase 정보와 함께 사진 올리기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/05-firebase/04/README.md)