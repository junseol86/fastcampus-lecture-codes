# 03. CSS 기본 스타일 1

## 글꼴, 글 관련 스타일
`상속`됨 : 부모의 스타일이 자식 스타일에도 적용

<br>

###  `font-family` : 글꼴 그룹
> 사용자의 컴퓨터에 지정한 폰트가 없을 때를 대비하여 여럿 지정
```css
  p { font-family: 돋움; }
  p { font-family: "맑은 고딕", 돋움, 굴림; }
```

<br>

#### `★` 웹 폰트 사용
 * <a href="https://fonts.google.com/" target="_blank">구글 폰트 사용해보기</a>

<br>
<br>

### `font-size` : 글자 크기  
| 단위 | 설명 | 비고 | 
| :-- | :-- | :-- | 
| `px` | 절대 크기: 모니터상의 한 점 | 사용자가 브라우저를 통해 조정 불가: **낮은 접근성** |
| `em` | 상대 크기: 바로 윗 부모의 크기에 비례 | 중첩마다 제곱 |
| `rem` | 상대 크기: 최상위 조상의 크기에 비례 | 일반적으로 body의 글자 크기: 16px |

```html
  일반 텍스트
  <span>바깥쪽 span
    <span>중간 span
      <span>안쪽 span
      </span>
    </span>
  </span>
```
```css
span { font-size: 24px; }
```
```css
span { font-size: 1.5em; }
```
```css
span { font-size: 1.5rem; }
```

<br>
<br>

### `font-weight` : 글자 굵기  
| 값 | 설명 | 비고 | 
| :-- | :-- | :-- | 
| `100 ~ 900` | 절대값 | 100 단위로 입력 |
| `normal` | 일반 굵기 | 400 |
| `bold` | 두껍게 | 700, 자주 사용됨 |
| `lighter`, `bolder` | 상속보다 얇거나 두껍게 | 정도는 상속된 값마다 다름 |

```css
span { font-weight: 900; }
```
```css
span { font-weight: bold; }
```
```css
body { font-weight: 100; }
span { font-weight: bolder; }
```

<br>
<br>

### `font-style` : 글자 스타일  
| 값 | 설명 | 비고 | 
| :-- | :-- | :-- | 
| `normal` | 일반 서체 |  |
| `italic` | 기울임 | 기울여 쓴 서체 - 주로 사용됨 |
| `oblique` | 기울임 | 본 서체를 기울인 것 |


<br>
<br>

### `color` : 글자 색

```html
  일반 텍스트 <span>글자색 적용</span>
```
```css
span { color: blue; }
```
```css
/* 빨강 + 초록 + 파랑 (0~255) */
span { color: rgb(0, 0, 255); }
```
```css
/* 빨강 + 초록 + 파랑 (0~255) + 불투명도 (0~1) */
span { color: rgba(0, 0, 255, 0.5); }
```
```css
/* 16진수 표기 */
span { color: #FF0000 }
```

<br>

> Google에 rgb(a)값, #hex 값 또는 `color picker` 검색

<br>
<br>

### `text-decoration` : 글자에 선 긋기

```html
  <span>일반</span>
  <span class="underline">밑줄</span>
  <span class="overline">윗줄</span>
  <span class="line-through">취소선</span>
```
```css
.none { text-decoration: none; }
.underline { text-decoration: underline; }
.overline { text-decoration: overline; }
.line-through { text-decoration: line-through; }
```

<br>
<br>

### `letter-spacing` : 자간 조정
```html
  일반 텍스트
  <span>자간이 조정된 텍스트</span>
```
```css
span { letter-spacing: 0.1em; }
```

<br>
<br>

### `text-align` : 텍스트 정렬
> block, inline-block, table 요소  

`left` | `right` | `center` | `justify`

```html
  <p>
    Cascading Style Sheets(CSS)는 HTML이나 XML(SVG, XHTML 같은 XML 방언(dialect) 포함)로 작성된 문서의 표현을 기술하기 위해 쓰이는 스타일시트 언어입니다. CSS는 요소가 화면, 종이, 음성이나 다른 매체 상에 어떻게 렌더링되어야 하는지 기술합니다.<br>
    CSS는 오픈 웹의 핵심 언어 중 하나이며 여러 브라우저가 표준으로 사용하는 W3C 명세가 있습니다. 레벨 단위로 개발한 CSS1은 더 이상 사용하지 않고, 다음 레벨인 CSS2.1은 권고안(recommendation)입니다. 더 작은 모듈로 나눈 CSS3은 표준화 과정을 밟고 있습니다.
  </p>
```
```css
/* left, right, center, justify */
p { text-align: left; }
```

<br>
<br>

### `line-height` : 행간  

```css
p { line-height: 24px; }
```
```css
p { line-height: 1.25; }
```
```css
p { line-height: 1.25em; }
```
```css
p { line-height: 125%; }
```

<br>
<br>

### `list-style` : ul 목록의 불릿 없애기

```html
  <ul>
    <li>못생긴</li>
    <li>불릿은</li>
    <li>이제 그만</li>
  </ul>
```
```css
ul { 
  list-style: none;
  padding: 0;
}
```

<br>
<br>

***

<br>
<br>

### `opacity` : 불투명도 
```html
  <span>오퍼시티가 적용된 요소</span>
```
```css
span {
  opacity: 0.5;
}
```

<br>
<br>

***

<br>
<br>

## 다음 강좌
* [04. CSS 기본 스타일 2](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/03-css/04/README.md)